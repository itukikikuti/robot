﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    [SerializeField] Transform leftLeg1;
    [SerializeField] Transform leftLeg2;
    [SerializeField] Transform target;
    [SerializeField] Transform joint;
    Quaternion initRotChain2;
    Quaternion initRotChain1;
    Quaternion initRotIkJoint;

    void Start()
    {
        initRotChain1 = leftLeg1.rotation;
        initRotChain2 = leftLeg2.rotation;
        initRotIkJoint = joint.rotation;
    }

    void Update()
    {
        if (Input.GetKey(KeyCode.A))
            transform.position += new Vector3(0.1f, 0f, 0f);

        if (Input.GetKey(KeyCode.D))
            transform.position += new Vector3(-0.1f, 0f, 0f);

        if (Input.GetKey(KeyCode.W))
            transform.position += new Vector3(0f, 0f, -0.1f);

        if (Input.GetKey(KeyCode.S))
            transform.position += new Vector3(0f, 0f, 0.1f);

        RaycastHit hit;
        if (Physics.Raycast(transform.position, Vector3.down, out hit))
        {
            transform.position += new Vector3(0f, 5f - hit.distance, 0f);
        }

        if (target.hasChanged)
        {
            //姿勢をリセット
            leftLeg1.rotation = initRotChain1;
            leftLeg2.rotation = initRotChain2;
            joint.rotation = initRotIkJoint;

            for (int index = 0; index < 4; ++index)
            {
                RotateForTarget(leftLeg1, joint, target);
                RotateForTarget(leftLeg2, joint, target);
            }
        }
        target.hasChanged = false;
    }

    void RotateForTarget(Transform chain, Transform ikJoint, Transform ikTarget)
    {
        //IKチェインからIKジョイントへのベクトル
        Vector3 toIkJoint = ikJoint.position;
        toIkJoint = chain.InverseTransformPoint(toIkJoint).normalized;
        //IKチェインからIKターゲットへのベクトル 
        Vector3 toIkTarget = ikTarget.position;
        toIkTarget = chain.InverseTransformPoint(toIkTarget).normalized;
        //回転軸 
        Vector3 axis = Vector3.Cross(toIkJoint, toIkTarget).normalized;
        //回転量 
        float amount = Vector3.Dot(toIkJoint, toIkTarget);
        //回転量をラジアンに変換
        float acos = Mathf.Acos(amount);
        //ラジアンを角度に変換
        float degree = acos * Mathf.Rad2Deg;
        chain.Rotate(axis, degree);
    }
}
